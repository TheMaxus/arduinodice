#include <Arduino.h>
#include <LiquidCrystal.h>

enum Pins { btn1 = 10, btn2 = 11, rs = 9, e = 8, ptn = A0 };

int diceMax[6] = {4, 6, 8, 10, 12, 20};
int currentDice = 0;
int diceAmount = 1;
bool btns_pre = false;

int ptn_pre = 0;

LiquidCrystal lcd(rs, e, 5, 4, 3, 2);

/**
 * Prints settings and program state on lcd
 *
 * @param rolled result of a dice roll
 */
void printSettings(int rolled) {
  lcd.setCursor(3, 0);
  lcd.print(" ");

  Serial.print(diceAmount);
  Serial.print("d");
  Serial.print(diceMax[currentDice]);

  lcd.setCursor(0, 0);
  lcd.print(diceAmount);
  lcd.setCursor(1, 0);
  lcd.print("d");
  lcd.setCursor(2, 0);
  lcd.print(diceMax[currentDice]);

  if (rolled != 0) {
    Serial.print(" ");
    Serial.print(rolled);

    lcd.setCursor(6, 1);
    lcd.print("   ");

    lcd.setCursor(0, 1);
    lcd.print("Roll:");
    lcd.setCursor(6, 1);
    lcd.print(rolled);
  }
  Serial.println();
}
/**
 * Rolls dice or multiple dices depending on diceAmout variable
 *
 * @return sum of rolled dices
 */
int rollDice() {
  int dice = 0;
  for (int i = 0; i < diceAmount; i++) {
    dice += random(0, diceMax[currentDice]) + 1;
  }
  return dice;
}

/**
 * Setup lcd, serial connection, buttons and random seed
 */
void setup() {
  lcd.begin(16, 2);
  Serial.begin(9600);
  pinMode(btn1, INPUT);
  pinMode(btn2, INPUT);

  randomSeed(analogRead(A5));
  printSettings(0);
}

/**
 * Main program loop
 */
void loop() {
  bool btn1_state = digitalRead(btn1);
  bool btn2_state = digitalRead(btn2);
  bool ptnChanged = abs(analogRead(ptn) - ptn_pre) > 5;

  if ((btn1_state && btn2_state) && !btns_pre) {
    printSettings(rollDice());
  }

  else if (btn1_state && ptnChanged) {
    currentDice = analogRead(ptn) / (1023 / 5);
    printSettings(0);
  }

  else if (btn2_state && ptnChanged) {
    diceAmount = analogRead(ptn) / (1023 / 8) + 1;
    printSettings(0);
  }

  ptn_pre = analogRead(A0);
  btns_pre = digitalRead(btn1) && digitalRead(btn2);

  delay(100);
}
